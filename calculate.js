function checkAndParseValue(value) {
    if (value !== "") {
        return parseFloat(value)
    }
    return value
}

function calculate(e){
    e.preventDefault()
    document.getElementById("error").value = ""
    var r = checkAndParseValue(document.getElementById("r").value)
    var ma = checkAndParseValue(document.getElementById("mal").value /180 * Math.PI)
    var A = checkAndParseValue(document.getElementById("A").value)
    var l = checkAndParseValue(document.getElementById("b").value)
    var c = checkAndParseValue(document.getElementById("s").value)
    var error = ""
    while (r === "" || ma === "" || A === "" || l === "" || c === "") {
        if(r !== "" && c !== "") {
            if(c > 2 * r){
                error = "Error: c > 2r"
            }
        }
        if(l !== "" && c !== "") {
            if(c >= l){
                error = "Error: c >= l"
            }
        }
        if(ma !== ""){
            if(ma === "0"){
                error = "Error: alpha = 0"
            }
            if(ma > Math.PI * 2){
                error = "Error: alpha > 360"
            }
        }
        if(l !== "" && r !== "") {
            if(l > 2 * r * Math.PI){
                error = "Error: l > 2 * pi * r"
            }
        }
        if (error !== "") {
            document.getElementById("error").value = error
            break;
        }
        if (r !== "" && ma !== 0) {
            if(A === ""){
                A = 1 / 2 * Math.pow(r, 2) * ma;
            }
            if(l === ""){
                l = r * ma;
            }
            if(c === ""){
                c = 2 * r * Math.sin(ma / 2);
            }
        } else if (r !== "") {
            if (A !== "") {
                ma = 2 * A / Math.pow(r, 2);
            } else if (l !== "") {
                ma = l / r;
            } else if (c !== "") {
                ma = Math.asin(c / (2 * r)) * 2;
            }
        } else if (ma !== 0) {
            if (A !== "") {
                r = Math.sqrt((2 * A) / ma);
            } else if (l !== "") {
                r = l / ma;
            } else if (c !== "") {
                r = 0.5 * c / Math.sin(ma / 2);
            }
        } else if (A !== "" && l !== "") {
            r = 2 * A / l
        } else if (A !== "" && c !== ""){
            let testA = 0;
            let w1;
            let w10;
            let w100;
            let w1000;
            let w10000;
            for(let i=Math.PI/180*100000 ; i<365*Math.PI/180*100000 ; i++){
                let w = i * 0.00001
                testA = 0.5 * Math.pow((0.5 * c / Math.sin(w/2)),2)*w;
                if(Math.round(A * 10000) / 10000 == Math.round(testA * 10000) / 10000){
                    w10000 = w;
                }
                else if(Math.round(A * 1000) / 1000 == Math.round(testA * 1000) / 1000){
                    w1000 = w;
                }
                else if(Math.round(A * 100) / 100 == Math.round(testA * 100) / 100){
                    w100 = w;
                }
                else if(Math.round(A * 10) / 10 == Math.round(testA * 10) / 10){
                    w10 = w;
                }
                else if(Math.round(A)==Math.round(testA)){
                    w1 = w;
                }
            }
            if(w10000){
                ma = w10000
            }
            else if(w1000){
                ma = w1000
            }
            else if(w100){
                ma = w100
            }
            else if(w10){
                ma = w10
            }
            else if(w1){
                ma = w1
            }
            if(ma === "" || ma === 0){
                error = "Error: alpha approach failed"
            }
        }
        else if (l !== "" && c !== ""){
            let testl = 0;
            let w1;
            let w10;
            let w100;
            let w1000;
            let w10000;
            for(let i=Math.PI/180*100000 ; i<365*Math.PI/180*100000 ; i++){
                let w = i * 0.00001
                testl = (c / (2 * Math.sin(w/2)) * w);
                if(Math.round(l * 10000) / 10000 == Math.round(testl * 10000) / 10000){
                    w10000 = w;
                }
                else if(Math.round(l * 1000) / 1000 == Math.round(testl * 1000) / 1000){
                    w1000 = w;
                }
                else if(Math.round(l * 100) / 100 == Math.round(testl * 100) / 100){
                    w100 = w;
                }
                else if(Math.round(l * 10) / 10 == Math.round(testl * 10) / 10){
                    w10 = w;
                }
                else if(Math.round(l)==Math.round(testl)){
                    w1 = w;
                }
            }
            if(w10000){
                ma = w10000
            }
            else if(w1000){
                ma = w1000
            }
            else if(w100){
                ma = w100
            }
            else if(w10){
                ma = w10
            }
            else if(w1){
                ma = w1
            }
            if(ma === "" || ma === 0){
                error = "Error: alpha approach failed"
            }
        }
        else{
            break;
        }
    }
    if(error == "") {
        document.getElementById("r").value = Math.round(r * 1000) / 1000
        document.getElementById("mal").value = Math.round((ma * 180 / Math.PI) * 1000) / 1000
        document.getElementById("A").value = Math.round(A * 1000) / 1000
        document.getElementById("b").value = Math.round(l * 1000) / 1000
        document.getElementById("s").value = Math.round(c * 1000) / 1000
        draw();
        let arr = []
        arr.push(document.getElementById("r"))
        arr.push(document.getElementById("mal"))
        arr.push(document.getElementById("A"))
        arr.push(document.getElementById("b"))
        arr.push(document.getElementById("s"))
        arr.forEach(function (obj){
            obj.disabled = true
        })
    }
}

function reset(){
    let ins = Array.prototype.slice.call(document.getElementsByClassName("numbers"))
    ins.forEach(function (item){
        item.value=""
    });
    document.getElementById("error").value = ""
    document.location.reload()
}
