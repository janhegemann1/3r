function drawCircle(){
    var c = document.getElementById("circle");
    var ctx = c.getContext("2d");
    ctx.strokeStyle = 'white';
    ctx.fillStyle = 'grey';
    ctx.lineWidth = 5;
    ctx.beginPath();
    ctx.arc(300, 300, 250, 0, 2 * Math.PI);
    ctx.closePath();
    ctx.stroke();
    ctx.fill();
    blockToManyInputs()
}

function draw(){
    let a = parseFloat(document.getElementById('mal').value.replace(",",".")).toFixed(3)
    let l = parseFloat(document.getElementById('b').value.replace(",",".")).toFixed(3)
    let r = parseFloat(document.getElementById('r').value.replace(",",".")).toFixed(3)
    let c = parseFloat(document.getElementById('s').value.replace(",",".")).toFixed(3)
    let A = parseFloat(document.getElementById('A').value.replace(",",".")).toFixed(3)
    if(a > 0 && l > 0 && r > 0 && c > 0 && A > 0)
    {
        var circle = document.getElementById("circle");
        var ctx = circle.getContext("2d");
        ctx.clearRect(0, 0, circle.width, circle.height);
        ctx.setLineDash([]);
        drawCircle()
        ctx.strokeStyle = 'white';
        ctx.fillStyle = '#0275d8';
        ctx.lineWidth = 5;
        ctx.beginPath();
        ctx.moveTo(300,300);
        ctx.arc(300,300,250,0,a/180*Math.PI);
        ctx.closePath();
        ctx.stroke();
        ctx.fill();
        ctx.lineWidth = 3;
        ctx.strokeStyle = 'white';
        ctx.beginPath();
        ctx.moveTo(300,300);
        ctx.arc(300,300,130,0,a/180*Math.PI);
        ctx.stroke();
        ctx.fillStyle = 'black';
        ctx.textAlign = 'center';
        ctx.textBaseline = "middle";
        if(document.getElementById("showNumbers").checked){
            var man = 'α = '+a+'°';
            var ln = 'l = '+l;
            var rn = 'r = '+r;
            var cn = 'c = '+c;
            var an = 'A = '+A
            ctx.font = '12px black';
        }
        else{
            var man = 'α';
            var ln = 'l';
            var rn = 'r';
            var cn = 'c';
            var an = 'A';
            ctx.font = '15px black';
        }
        ctx.moveTo(300, 300);
        ctx.lineTo(300, 50);
        ctx.stroke();
        ctx.setLineDash([15, 15]);
        ctx.moveTo(550, 300);
        ctx.lineTo((250*Math.cos(a*Math.PI/180))+300, (250*Math.sin(a*Math.PI/180))+300);
        cxm = 550 + (((250*Math.cos(a*Math.PI/180))+300 - 550) / 2)
        cym = (((125*Math.sin(a*Math.PI/180))+320))
        ctx.stroke();
        ctx.fillText(an, (160*Math.cos(a/2*Math.PI/180))+300, (160*Math.sin(a/2*Math.PI/180))+300);
        ctx.fillText(cn, cxm, cym);
        ctx.fillText(man, (75*Math.cos(a/2*Math.PI/180))+300, (75*Math.sin(a/2*Math.PI/180))+300);
        ctx.fillText(ln, (280*Math.cos(a/2*Math.PI/180))+310, (280*Math.sin(a/2*Math.PI/180))+300);
        ctx.fillText(rn, 330, 175);
        ctx.closePath();
    }
}

function blockToManyInputs(){
    let arr = []
    let empty = []
    let count = 0
    btn = document.getElementById("calcBtn")
    btn.disabled = true
    arr.push(document.getElementById("r"))
    arr.push(document.getElementById("mal"))
    arr.push(document.getElementById("A"))
    arr.push(document.getElementById("b"))
    arr.push(document.getElementById("s"))
    arr.forEach(function (obj){
        if(obj.value !== ""){
            count++
        }
        else {
            empty.push(obj)
        }
    })
    if(count == 2){
        empty.forEach(function (obj){
            obj.disabled = true
            btn.disabled = false
        })
    }
    else{
        arr.forEach(function (obj){
            obj.disabled = false
        })
    }
}